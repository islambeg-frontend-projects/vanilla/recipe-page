<h1 align="center">Recipe Page</h1>

<div align="center">
   Solution for a challenge from  <a href="http://devchallenges.io" target="_blank">Devchallenges.io</a>.
</div>

<div align="center">
  <h3>
    <a href="https://islambeg-frontend-projects.gitlab.io/vanilla/recipe-page">
      Demo
    </a>
    <span> | </span>
    <a href="https://gitlab.com/islambeg-frontend-projects/vanilla/recipe-page">
      Solution
    </a>
    <span> | </span>
    <a href="https://devchallenges.io/challenges/OEKdUZ6xs0h99C38XVht">
      Challenge
    </a>
  </h3>
</div>

## Table of Contents

- [Overview](#overview)
  - [Built With](#built-with)
- [Acknowledgements](#acknowledgements)
- [Contact](#contact)

## Overview

Simple HTML+CSS recipe page.

### Built With

- HTML
- CSS
- [Sass](https://sass-lang.com/)

## Acknowledgements

- [A modern CSS Reset by Andy Bell](https://piccalil.li/blog/a-modern-css-reset/)
- [Pure CSS Custom Checkbox Style by Stephanie Eckles](https://moderncss.dev/pure-css-custom-checkbox-style/)
- [Icons by Google](https://google.github.io/material-design-icons/)

## Contact

- Email islambeg@proton.me
- Gitlab [@islambeg](https://gitlab.com/islambeg)
