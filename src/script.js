const referenceDesktopVW = 1440;
const referenceMobileVW = 375;
const htmlEl = document.documentElement;

function getFontChangeRate(currentVW, referenceVW) {
  return currentVW <= referenceVW ? 0.6 : 1;
}

function onResize() {
  const currentVW = window.innerWidth;
  const referenceVW = currentVW <= 700 ? referenceMobileVW : referenceDesktopVW;
  const fontChangeRate = getFontChangeRate(currentVW, referenceVW);
  const baseFontMultiplier = Math.pow(currentVW / referenceVW, fontChangeRate);
  const newBaseFontRatio = baseFontMultiplier * 62.5;
  htmlEl.style.fontSize = `${newBaseFontRatio.toFixed(4)}%`;
}

window.addEventListener("resize", onResize);
onResize();
